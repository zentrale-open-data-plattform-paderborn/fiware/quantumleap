# Quantumleap

## General
Data that arrives to Orion Context Broken, can be stored as historical data in QuantumLeap via subscription mechanism. QuantumLeap is the first implementation of an API that supports the storage of NGSI FIWARE NGSIv2 data into a time-series database, known as ngsi-tsdb. Accessed via NGSI v2 API.

When a subscription is registered at context broker, broker will POST data to QuantumLeap's notification endpoint. QuantumLeap will then push the information for storage to CrateDB.

Original documentation can be found here: https://quantumleap.readthedocs.io

## Usage of QuantumLeap
Use of this service and CrateDB database is handled by the components themselves, so for basic use of the system the user does not need know how to operate this after it has been configured.  
Learn more from the official documentation: https://quantumleap.readthedocs.io/en/latest/admin/configuration/

## Versioning  
Tested on:  
QuantumLeap version smartsdk/quantumleap:0.7.5 (DockerHub digest: sha256:0b9c67b11209809865c556db2c9ceb82839f334399518bb84000a05a5f0be114)  
CrateDB version crate:3.3.5 (DockerHub digest: sha256:1220d3a66ba7434b40edd63308e7ebb931f7d20dfc03a7f40f24846525d96261)

## Volume Usage
QuantumLeapCrate uses a persistent directory to store the data (/data)  
PVC: NAMESPACE-cratedata-STATEFULSET-NAME

## Load-Balancing
By default, five replicas of Quantumleap and three replicas of CrateDB are deloyed.

CrateDB replicas act as one cluster of three nodes.

## Route (DNS)
Quantumleap can be connected to via: https://sthdata.fiware.opendata-CITY.de (No GUI available)

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
### Kubernetes Secrets
This project does not use Kubernetes secrets.

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
